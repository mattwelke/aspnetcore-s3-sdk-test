﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AspCoreS3Test
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Register AWS region globally in app for any AWS clients that need it.
            services.AddSingleton<RegionEndpoint>(_ => RegionEndpoint.CACentral1);

            // Register S3 client for each request.
            services.AddScoped<IAmazonS3, AmazonS3Client>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                // Fetch an S3 client for the HTTP request
                var s3Client = context.RequestServices.GetService<IAmazonS3>();

                // Use the client to fetch the buckets.
                var buckets = await s3Client.ListBucketsAsync();

                // Map the info about the buckets to an HTML response.
                var bucketNameListItems = buckets.Buckets.Select(bucket =>
                {
                    return $"<li>{bucket.BucketName}</li>";
                });

                var response = "<h1>Buckets in Canada Region:</h1><ul>";
                foreach (var b in bucketNameListItems)
                {
                    response += $"<li>{b}</li>";
                }
                response += "</ul>";

                // Send the response.
                await context.Response.WriteAsync(response);
            });
        }
    }
}
